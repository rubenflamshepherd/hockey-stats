import sqlite3
import matplotlib.pyplot as plt
import pandas as pd
import pickle


def update_table():
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    c.execute(
        'DELETE FROM nhl_player_seasons WHERE year=20042005')
    conn.commit()
    c.close



def toi_to_min(temp_df):
    """ Convert toi str to float in seconds

    :param toi_str:
    :return: float
    """

    minutes, seconds = temp_df['toi_gp'].split(':')


def _parse_season_int(curr_tuple):
    year_raw = curr_tuple[2]
    first_year = year_raw[0:4]
    return int(first_year)


def id_rookie_seasons(search_limiter):
    """Find rookie seasons in our data base. last_year is the the oldest a season can be

     :param search_limiter: 4 digit int
    :return: [tuple]
    """
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    c.execute(
        'SELECT * FROM nhl_player_pages')
    all_players = c.fetchall()
    # Grab all rookie seasons
    rookie_seasons = []
    for player in all_players:
        playerid = player[0]
        c.execute(
            'SELECT * FROM nhl_player_seasons WHERE id={}'.format(playerid))
        all_seasons = c.fetchall()
        # Find rookie season
        rookie_season = None
        rookie_year = 3000
        for curr_season in all_seasons:
            curr_year = _parse_season_int(curr_season)
            if curr_year < rookie_year:
                rookie_year = curr_year
                rookie_season = curr_season
        # Add rookie season if its recent enough and not None
        if rookie_year >= search_limiter and rookie_season:
            rookie_seasons.append(rookie_season)

    return rookie_seasons


def get_gross_season_stats(year):
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    df = pd.read_sql_query(
        'SELECT * FROM nhl_player_seasons WHERE year={} AND season_type={} AND gp>30'.format(year, '2'), con=conn)
    df['minutes_raw'] = df['toi_gp'].str.split(':')
    df['minutes'] = df['minutes_raw'].str.get(0).astype(int)
    df['seconds_raw'] = df['toi_gp'].str.split(':')
    df['seconds'] = df['seconds_raw'].str.get(1).astype(int)
    df['seconice_gp'] = (df['minutes'] * 60) + df['seconds']
    df['seconice_tot'] = df['seconice_gp'] * df['gp']
    df['minonice_tot'] = df['seconice_tot'] / 60
    df['g_hr'] = (df['goals'] / df['seconice_tot'] * 60 * 60).round(3)
    df['a_hr'] = (df['assists'] / df['seconice_tot'] * 60 * 60).round(3)
    df['pts_hr'] = (df['points'] / df['seconice_tot'] * 60 * 60).round(3)
    df['shts_hr'] = (df['s'] / df['seconice_tot'] * 60 * 60).round(3)
    df['goals_rank'] = df['goals'].rank(ascending=False)
    df['g_hr_rank'] = df['g_hr'].rank(ascending=False)
    df['a_rank'] = df['a_hr'].rank(ascending=False)
    df['pts_rank'] = df['pts_hr'].rank(ascending=False)
    df['shts_rank'] = df['shts_hr'].rank(ascending=False)

    conn.close()

    return df.loc[:,
           [
               'id', 'name', 'year', 'goals', 'assists', 's', 'points', 'pts_hr', 'seconice_tot', 'g_hr', 'g_hr_rank',
               'goals_rank', 'a_rank', 'pts_rank', 'shts_hr', 'shts_rank', 'minonice_tot', 'gp', 'pos'
           ]
    ]


if __name__ == '__main__':

    temp_rookie_seasons = id_rookie_seasons(2000)
    years = [
        '20002001', '20012002', '20022003', '20032004', '20042005', '20052006', '20062007', '20072008', '20082009',
        '20102011', '20112012', '20122013', '20132014', '20142015', '20152016', '20162017'
    ]
    frames = []
    for year in years:
        temp = get_gross_season_stats(year)
        frames.append(temp)
    df = pd.concat(frames)

    rookie_frames_lst = []
    for rookie_season in temp_rookie_seasons:
        id_, name, curr_year = rookie_season[0], rookie_season[1], rookie_season[2]
        new_df = df.loc[(df['id'] == id_) & (df['name'] == name) & (df['year'] == curr_year)]
        if len(new_df) == 1:
            rookie_frames_lst.append(new_df)
        elif len(new_df) >= 1:
            assert False, 'Trouble converting rookie season to DataFrame'
    rookie_frames = pd.concat(rookie_frames_lst)

    writer = pd.ExcelWriter('PythonExport.xlsx')

    goals_frames = rookie_frames.loc[:, ['name', 'year', 'goals', 'goals_rank', 'g_hr']]
    goals_frames = goals_frames.sort_values('goals', ascending=False)
    g_ranks_frames = rookie_frames.loc[:, ['name', 'year', 'goals_rank', 'goals']]
    g_ranks_frames = g_ranks_frames.sort_values('goals_rank', ascending=True)
    minonice_frames = rookie_frames.loc[:, ['name', 'year', 'minonice_tot', 'pos']]
    minonice_frames = minonice_frames[minonice_frames.pos != 'D']
    minonice_frames = minonice_frames.sort_values('minonice_tot', ascending=False)
    points_rank_frames = rookie_frames.loc[:, ['name', 'year', 'points', 'pts_rank', 'pts_hr']]
    points_rank_frames = points_rank_frames.sort_values('pts_rank', ascending=True)
    points_hr_frames = rookie_frames.loc[:, ['name', 'year', 'points', 'pts_rank', 'pts_hr', 'minonice_tot']]
    points_hr_frames = points_hr_frames.sort_values('pts_hr', ascending=False)
    shots_hr_frames = rookie_frames.loc[:, ['name', 'year', 'shts_rank', 'shts_hr', 's']]
    shots_hr_frames = shots_hr_frames.sort_values('shts_rank', ascending=True)

    goals_frames.to_excel(writer, 'Goals')
    g_ranks_frames.to_excel(writer, 'Goals Ranks')
    minonice_frames.to_excel(writer, 'TOI')
    points_rank_frames.to_excel(writer, 'Pts Rank')
    points_hr_frames.to_excel(writer, 'Pts per Hr Rank')
    shots_hr_frames.to_excel(writer, 'Shots per Hr Rank')
    writer.save()
    print(rookie_frames)

    # with open('rookie_frames.pickle', 'wb') as handle:
    #     pickle.dump(rookie_frames, handle, protocol=pickle.HIGHEST_PROTOCOL)
    # with open('rookie_frames.pickle', 'rb') as handle:
    #        rookie_frames = pickle.load(handle)
    #print(temp)
    #plt.hist(temp.gp.astype('int'), bins=100)
    #plt.show()

