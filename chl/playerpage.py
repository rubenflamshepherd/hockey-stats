import sqlite3
import os
import time
import datetime

from random import randint
from selenium import webdriver


class UnrecognizedHeightError(Exception):
    pass


class UnrecognizedWeightError(Exception):
    """Base class for exceptions in this module."""
    pass


class Birthplace:

    def __init__(self, city, state, country):
        self.city = citypi
        self.state = state
        self.country = country

    def __str__(self):
        return self.city + " " + str(self.state) + " " + self.country


class Draft:

    def __init__(self, league, year, team, draft_round, overall, signing_type):
        self.league = league
        self.year = year
        self.team = team
        self.round = draft_round
        self.overall = overall
        self.signing_type = signing_type

    def __str__(self):
        return self.league + " " + self.year + " " + self.team + " " + self.round + " " + self.overall


class PlayerPage:
    """Object representing a single player.
    """
    def __init__(
            self, id_, league, name, num, pos, height, weight, birthdate, birthplace, shoots, drafts):
        self.id = id_
        self.league = league
        self.name = name
        self.num = num
        self.pos = pos
        self.height = height
        self.weight = weight
        self.birthdate = birthdate
        self.birthplace = birthplace
        self.shoots = shoots
        self.drafts = drafts

    def __str__(self):
        return "{:<25}".format("|Name " + self.name) + \
               "{:<15}".format("|ID " + self.id) + \
               "{:<10}".format("|Pos " + self.pos) + \
               "{:<10}".format("|Born in  " + str(self.birthplace))


def _update_table():
    """Utility function for updating records

    :return: None
    """
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    c.execute('UPDATE chl_drafts SET overall=73 WHERE id="6204"')
    conn.commit()
    conn.close()


def _create_player_draft_table():
    """Utility function for creating/initializing the chl_drafts table

    :return: None
    """
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    c.execute('DROP TABLE IF EXISTS chl_drafts')
    c.execute('''CREATE TABLE chl_drafts
                 (
                 id TEXT, league TEXT, year INT, team TEXT, round INT, overall INT, type TEXT,
                 PRIMARY KEY (id, league, year)
                 )
                 ''')
    conn.commit()
    conn.close()


def _create_player_pages_table():
    """Utility function for creating/initializing the chl_player_pages table

    :return: None
    """
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    c.execute('DROP TABLE IF EXISTS chl_player_pages')
    c.execute('''CREATE TABLE chl_player_pages
                 (
                 id TEXT, league TEXT, name TEXT, num TEXT, pos TEXT, height REAL, weight REAL, birth_date TEXT,
                 birth_city TEXT, birth_state TEXT, birth_country TEXT, shoots TEXT,
                 PRIMARY KEY (id, league)
                 )
                 ''')
    conn.commit()
    conn.close()


def _save_player_page(db_cursor, player_page):
    """ Save a list of PlayerSeason objects to a database

    :param c: database cursor
    :param player_page: PlayerPage
    :return: None
    """
    temp_player_page = (
        player_page.id, player_page.league, player_page.name, player_page.num, player_page.pos, player_page.height,
        player_page.weight, str(player_page.birthdate), player_page.birthplace.city, player_page.birthplace.state,
        player_page.birthplace.country, player_page.shoots,
    )
    db_cursor.execute(
        'INSERT INTO chl_player_pages VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        temp_player_page)

    for draft in player_page.drafts:
        temp_draft = (
            player_page.id, draft.league, draft.year, draft.team, draft.round, draft.overall, draft.signing_type)
        try:
            db_cursor.execute(
                'INSERT INTO chl_drafts VALUES (?, ?, ?, ?, ?, ?, ?)',
                temp_draft)
        except sqlite3.IntegrityError:
            print('duplicate draft entry.....')
    print(" saved")


def _player_exists(db_cursor, player_id):
    """Return whether or not the given player has already been grabbed

    :param db_cursor: database cursor
    :param player_id:  int
    :return: bool
    """
    checker = db_cursor.execute(
        'SELECT * FROM chl_player_pages WHERE id=?',
        (player_id,))
    if len(checker.fetchmany()) == 0:
        return False
    else:  # len(checker) >= 1
        print(" already saved!")
        return True


def _ft_to_cm(ft, inches):
    total_inches = ft * 12 + inches
    total_cm = total_inches * 2.54
    if total_cm == 0:
        return None
    else:
        return total_cm


def _parse_birthdate(birth_str_raw):
    birth_str_raw = birth_str_raw.strip()
    birth_str = birth_str_raw.split('-')
    year = int(birth_str[0])
    month = int(birth_str[1])
    day = int(birth_str[2])
    return datetime.date(year, month, day)


def _parse_birthplace(birthplace_str):
    state, country = None, None  # Default values
    birthplace_str = birthplace_str.strip()
    birthplace_str = birthplace_str.split(',')[0:2]
    city, territory = birthplace_str
    if territory.isupper():
        state = territory.strip()
    else:
        country = territory.strip()
    return Birthplace(city, state, country)


def _parse_nums(str_with_nums):
    nums = ''
    for item in str_with_nums:
        if item.isdigit():
            nums += item
    return nums


def _parse_height(height_raw):
    """

    :param height_raw: str
    :return: str
    """

    if height_raw.find('.') != -1:
        splitting_char = '.'
    elif height_raw.find("'") != -1:
        splitting_char = "'"
    else:
        raise UnrecognizedHeightError
    feet_raw, inches_raw = height_raw.split(splitting_char)
    feet = int(feet_raw.strip())
    inches = int(inches_raw.strip().strip('"'))
    return feet, inches


def _parse_draft(draft_str):
    draft_list = draft_str.split()
    league = draft_list[0]
    team = draft_list[2]
    signing_type = None
    year, round_, overall = None, None, None
    if draft_list[3] == 'FA' or draft_list[3].upper() == 'TR.' or draft_list[3] == 'TRADE':
        signing_type = draft_list[3].upper()
        year = draft_list[4].strip('(').strip(')')
    elif draft_list[2] == 'AP':
        team = draft_list[3]
        signing_type = draft_list[2].upper()
    else:
        year = draft_list[3].strip('(').strip(')')
        round_str = draft_list[-2]
        overall_str = _parse_nums(draft_list[-1])
        round_ = int(round_str)
        try:
            overall = int(overall_str)
        except ValueError:
            overall = None

    return Draft(league, year, team, round_, overall, signing_type)


def _parse_primary_element(primary_element):
    """
    :param primary_element: WebDriver
    :return: str, str, str
    """
    name_raw = primary_element.find_element_by_class_name('player-profile-info__full-name')
    name = name_raw.text
    num_raw = primary_element.find_element_by_class_name('player-profile-info__number')
    num = _parse_nums(num_raw.text)
    pos_raw = primary_element.find_element_by_class_name('player-profile-info__position')
    pos = pos_raw.text
    return name, num, pos


def _parse_secondary_element(secondary_element, league):
    """

    :param secondary_element: WedDriver
    :param league: str
    :return: float, float, datetime, BirthPlace, str,
    """
    drafts = []  # Default value
    elements_raw = secondary_element.find_elements_by_class_name('player-profile-info')
    for item_raw in elements_raw:
        contents = item_raw.text.split(':')
        header, stat = contents[0], contents[1]
        if 'Shoots' in header:
            shoots = stat.strip()
        elif 'Height' in header:
            try:
                feet, inches = _parse_height(stat)
                height_raw = _ft_to_cm(feet, inches)
                height = round(height_raw, 3)
            except UnrecognizedHeightError:
                height = None
        elif 'Weight' in header:
            try:
                weight_raw = int(stat.strip()) * 0.453592
                weight = round(weight_raw, 3)
            except ValueError:
                print('unrecognized weight....')
                weight = None
        elif 'Birthdate' in header:
            try:
                birthdate = _parse_birthdate(stat)
            except ValueError:
                print('unrecognized birthdate....')
                birthdate = None
        elif 'Hometown' in header:
            birthplace = _parse_birthplace(stat)
        elif 'Draft' in header:
            draft_raw = item_raw.find_elements_by_tag_name('div')[1]
            draft_raw = draft_raw.find_elements_by_tag_name('div')
            for item in draft_raw:
                drafts.append(_parse_draft(item.text))
    return height, weight, birthdate, birthplace, shoots, drafts


def _parse_player_page(league, url_prefix, id_, curr_driver):
    """Given a WebDriver <driver>, point the driver to a player page at url_prefix with <id_> and return the
     PlayerPage object.

    :param league: str
    :param url_prefix: str
    :param id_: str
    :param currdriver: WebDriver
    :return: PlayerPage
    """
    url_complete = url_prefix + "/players/" + id_

    curr_driver.get(url_complete)
    # Get WebDriver containing info
    primary_element = curr_driver.find_element_by_class_name('player-profile-primary')
    secondary_element = curr_driver.find_element_by_class_name('player-profile-secondary')

    name, num, pos = _parse_primary_element(primary_element)
    height, weight, birthdate, birthplace, shoots, drafts =\
        _parse_secondary_element(secondary_element, league)

    return PlayerPage(id_, league, name, num, pos, height, weight, birthdate, birthplace, shoots, drafts)


def save_player_pages(cap):
    driver = webdriver.Chrome(executable_path=os.path.join(os.getcwd(), "driver\chromedriver.exe"))
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    c.execute(
        'SELECT * FROM chl_player_seasons')
    all_seasons = c.fetchall()
    num_seasons = len(all_seasons)

    start_time = time.time()
    page_counter = 0

    counter = 0
    while counter < cap and counter < num_seasons:
        curr_player_season = all_seasons[counter]
        curr_player_id = curr_player_season[1]
        curr_player_name = curr_player_season[5]
        print('{0:.<40}'.format('Examining ' + curr_player_id + " " + curr_player_name), end='')
        if not _player_exists(c, curr_player_id):
            temp_player_page = _parse_player_page('OHL', 'http://ontariohockeyleague.com', curr_player_id, driver)
            _save_player_page(c, temp_player_page)
            page_counter += 1
            conn.commit()
            time.sleep(randint(1, 5))
        counter += 1

    total_time = time.time() - start_time
    if page_counter == 0:
        time_per_page = 0
    else:
        time_per_page = total_time/page_counter
    print("That took " + str(total_time) + " seconds")
    print(str(page_counter) + " pages saved. " + str(time_per_page) + " seconds per page")

    driver.close()
    conn.close()

if __name__ == '__main__':
    # _create_player_draft_table()
    # _create_player_pages_table()
    save_player_pages(3000)
    # _update_table()

    '''
    driver = webdriver.Chrome(executable_path=os.path.join(os.getcwd(), "driver\chromedriver.exe"))
    temp_player = _parse_player_page('OHL', 'http://ontariohockeyleague.com', '1906', driver)
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    _save_player_page(c, temp_player)
    conn.commit()
    driver.close()
    conn.close()
    '''

