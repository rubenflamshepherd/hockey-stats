import pandas as pd
import numpy as np
import sqlite3
import matplotlib.pyplot as plt

tables = ['chl_drafts', 'chl_player_pages', 'chl_player_seasons',
          'nhl_player_pages', 'nhl_player_seasons']


def create_data_matrix(title='nhl_player_seasons'):
    con = sqlite3.connect('hockey-stats.db')
    df = pd.read_sql_query('select * from {} WHERE num IS NOT NULL'.format(title), con=con)
    return df


df = create_data_matrix(title=tables[3])

print(df.num.astype('int'))
plt.hist(df.num.astype('int'), bins=100)
plt.show()
