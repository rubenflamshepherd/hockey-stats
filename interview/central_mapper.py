import os
import sqlite3

import geocoder
from geopy.distance import vincenty
import nvector
import pandas as pd
import plotly
import pickle
from selenium import webdriver


def get_players(team_name, from_pickle):
    """Given a <team_name>, navigate to url with all rosters, grab player names of <team_name> and return name.

    :param team_name: str
    :param from_pickle: Bool
    :return: [str]
    """
    if from_pickle:
        with open('interview\players.pickle', 'rb') as handle:
            return pickle.load(handle)
    else:
        url = "https://www.nhl.com/news/world-cup-of-hockey-rosters/c-280810684"
        driver = webdriver.Chrome(executable_path=os.path.join(os.getcwd(), "driver\chromedriver.exe"))
        driver.get(url)
        temp_webdrivers = driver.find_elements_by_tag_name('p')
        counter = 0
        for section in temp_webdrivers:
            if section.text.strip() == team_name:
                players, players_raw = [], []
                forwards = temp_webdrivers[counter + 2]
                defensemen = temp_webdrivers[counter + 4]
                goalies = temp_webdrivers[counter + 6]
                players_raw += forwards.text.split(';')
                players_raw += defensemen.text.split(';')
                players_raw += goalies.text.split(';')
                for item in players_raw:
                    name, team_country = item.split(',')
                    players.append(name.strip().lower())
                driver.close()
                with open('interview\players.pickle', 'wb') as handle:
                    pickle.dump(players, handle, protocol=pickle.HIGHEST_PROTOCOL)
                return players
            counter += 1


def get_birthplaces(players, extra_places):
    """Given a list of player names, find their birthplaces in database and return them

    :param players: [str]
    :param extra_places: [str]
    :return: [str]
    """
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    birthplaces = []

    for name in players:
        c.execute(
            'SELECT * FROM nhl_player_pages WHERE name=? AND draft_year>"1979"',
            (name,))
        temp_player = c.fetchall()
        assert len(temp_player) <= 1, 'More than one player with name {}'.format(name)
        try:
            birth_locals_raw = temp_player[0]
            birth_locals_raw = birth_locals_raw[7:10]
            birth_locals = []
            for item in birth_locals_raw:
                if item is not None:
                    birth_locals.append(item)
            birthplace_str = ', '.join(birth_locals)
            birthplaces.append(birthplace_str)
        except IndexError:
            print('{} has no entry'.format(name))

    for item in extra_places:
        birthplaces.append(item)

    return birthplaces


def _update_database():
    conn = sqlite3.connect('hockey-stats.db')
    c = conn.cursor()
    c.execute('UPDATE nhl_player_pages SET name = LOWER(name)')
    conn.commit()
    conn.close()


def get_geographic_center(lats, longs):
    """ Given a list of latitides and longitudes (paired), return the latititude/longitude corresponding to their
     geographic center

    :param lats: [float]
    :param longs: [floats]
    :return: float, float
    """
    points = nvector.GeoPoint(latitude=lats, longitude=longs, degrees=True)
    nvectors = points.to_nvector()
    n_EM_E = nvectors.mean_horizontal_position()
    g_EM_E = n_EM_E.to_geo_point()
    lat, long = g_EM_E.latitude_deg, g_EM_E.longitude_deg
    return lat[0], long[0]


def get_closest_point(central_point, comparison_points):
    """ Return the comparison points that is closes to the central point

    :param central_point:
    :param comparison_points:
    :return:
    """
    closest_distance = 40075  # Circumference of earth
    closest_point = (0, 0)
    closest_city = ''
    for curr_item in comparison_points:
        curr_point = (curr_item[0], curr_item[1])
        curr_city = curr_item[2]
        curr_distance = vincenty(central_point, curr_point).kilometers
        if curr_distance < closest_distance:
            closest_distance = curr_distance
            closest_point = curr_point
            closest_city = curr_city

    return closest_point[0], closest_point[1], closest_city


def get_lats_longs(places, from_pickle, file_name):
    """Using GoogleMaps API, convert list of places to list of latitudes/longitudes. <file_name> is used to create
    pickle file.

    :param places: [str]
    :param from_pickle: Bool
    :param file_name: Bool
    :return: [float], [float]
    """
    if from_pickle:
        with open('interview\lats_{}.pickle'.format(file_name), 'rb') as handle:
            lats = pickle.load(handle)
        with open('interview\longs_{}.pickle'.format(file_name), 'rb') as handle:
            longs = pickle.load(handle)
        return lats, longs
    else:
        lats, longs = [], []
        for place in places:
            temp_place = geocoder.google(place, key='AIzaSyC_Kq1AT0T - Pl4lValIIHN4xsu4YxSzdxA')
            lats.append(temp_place.latlng[0])
            longs.append(temp_place.latlng[1])
        with open('interview\lats_{}.pickle'.format(file_name), 'wb') as handle:
            pickle.dump(lats, handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open('interview\longs_{}.pickle'.format(file_name), 'wb') as handle:
            pickle.dump(longs, handle, protocol=pickle.HIGHEST_PROTOCOL)
        return lats, longs


def create_data(team, comparison_cities, goalie_birthplaces, from_pickle):
    geo_comparison = {'cities': comparison_cities}
    geo_comparison['lat'], geo_comparison['long'] = get_lats_longs(
        geo_comparison['cities'], from_pickle, file_name='comparison')

    geo_data = {'players': get_players(team, from_pickle)}

    geo_data['birthplaces'] = get_birthplaces(geo_data['players'], goalie_birthplaces)
    geo_data['lat'], geo_data['long'] = get_lats_longs(
        geo_data['birthplaces'], from_pickle, file_name='birthplaces')

    geo_dataframe = pd.DataFrame(geo_data)
    comparison_dataframe = pd.DataFrame(geo_comparison)
    comparison_dataframe['text'] = comparison_dataframe['cities']

    df = geo_dataframe
    df['text'] = df['birthplaces'] + ' | ' + df['players']
    # Calculate the geographic center
    center_lat, center_long = get_geographic_center(df['lat'], df['long'])
    # Calulcate clostest comparison city
    comparison_tuple = [(row['lat'], row['long'], row['cities']) for index, row in comparison_dataframe.iterrows()]
    closest_lat, closest_long, closest_city = get_closest_point(
        (center_lat, center_long), comparison_tuple)

    data = [
        # Graph player birthplaces
        dict(
            type='scattergeo',
            name='Player birthplaces',
            lon=df['long'],
            lat=df['lat'],
            text=df['text'],
            mode='markers',
            marker=dict(
                size=10,
                opacity=0.8,
                reversescale=True,
                autocolorscale=False,
                symbol='circle',
                color='rgb(0, 112, 192)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'
                )
            )),
        # Graph comparison cities
        dict(
            type='scattergeo',
            name='Comparison cities',
            lon=comparison_dataframe['long'],
            lat=comparison_dataframe['lat'],
            text=comparison_dataframe['text'],
            mode='markers',
            marker=dict(
                size=10,
                opacity=0.8,
                reversescale=True,
                autocolorscale=False,
                symbol='square',
                color='rgb(146, 208, 80)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'
                )
            )),
        # Graph closest cities
        dict(
            type='scattergeo',
            name='Closest city',
            lon=[closest_long],
            lat=[closest_lat],
            text=[closest_city],
            mode='markers',
            marker=dict(
                size=10,
                opacity=0.8,
                reversescale=True,
                autocolorscale=False,
                symbol='square',
                color='rgb(255, 0, 0)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'
                )
            )),
        # Graph simple geographic center
        dict(
            type='scattergeo',
            name='Mean lat/long of players',
            lon=[df['long'].mean()],
            lat=[df['lat'].mean()],
            text=['Average lat/long of all players'],
            mode='markers',
            marker=dict(
                size=10,
                opacity=0.8,
                reversescale=True,
                autocolorscale=False,
                symbol='square',
                color='rgba(152, 0, 0, .8)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'
                )
            )),
        # Graph correct geographic center
        dict(
            type='scattergeo',
            name='Geographic center',
            lon=[center_long],
            lat=[center_lat],
            text=['Geographic center'],
            mode='markers',
            marker=dict(
                size=10,
                opacity=0.8,
                reversescale=True,
                autocolorscale=False,
                symbol='square',
                color='rgb(0, 0, 0)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'
                )
            )),
    ]
    return data


def create_layout(team):
    layout = dict(
        title='Geographic Center of {} Birthplaces'.format(team.title()),
        colorbar=True,
        legend=dict(orientation="h"),
        geo=dict(
            projection=dict(type='Mercator'),
            showland=True,
            landcolor="rgb(250, 250, 250)",
            subunitcolor="rgb(217, 217, 217)",
            countrycolor="rgb(217, 217, 217)",
            countrywidth=0.5,
            subunitwidth=0.5,
            showsubunits=True,
            showcountries=True,
        ),
    )
    return layout


if __name__ == '__main__':
    team = 'TEAM USA'
    temp_data = create_data(
        team=team,
        comparison_cities=['Washington, DC, USA', 'Kansas City, USA', 'Hamilton, ON, CAN', 'Great Bend, USA'],
        goalie_birthplaces=['Denver, CO, USA', 'Milford, CT, USA', 'Marblehead, MA, USA'],
        from_pickle=False)
    '''
    team = 'TEAM NORTH AMERICA'
    temp_data = create_data(
        team=team,
        comparison_cities=['Regina, CAN', 'Washington, DC, USA', 'Kansas City, USA', 'Hamilton, ON, CAN'],
        goalie_birthplaces=['Pittsburgh, USA', 'Commerce, USA', 'Thunder Bay, CAN'],
        from_pickle=False)
    team = 'TEAM EUROPE'
    temp_data = create_data(
        team=team,
        comparison_cities=['Salzburg, AUT', 'Nuremberg, DEU', 'Zurich, CHE', 'Prague, CZE'],
        goalie_birthplaces=['Fussen, DEU', 'Rosenheim, DEU', 'Bratislava, SVK'],
        from_pickle=False)
    team = 'TEAM CANADA'
    temp_data = create_data(
        team=team,
        comparison_cities=['Montreal, CAN', 'Lloydminster, CAN', 'Anahim Lake, CAN'],
        goalie_birthplaces=['Montreal, CAN', 'Lloydminster, CAN', 'Anahim Lake, CAN'],
        from_pickle=False)

    '''
    temp_layout = create_layout(team)
    fig = dict(data=temp_data, layout=temp_layout)
    plotly.plotly.plot(fig, validate=False, filename=team)

